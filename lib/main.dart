//Berikut ini adalah projek milik
//Nama : Jovan Febrian Limanto
//NIM : 55200232

import 'dart:math';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      //di atas untuk hilangin banner!
      //di bawah ini untuk pindah halaman.
      home: halamanU(),
      routes: <String, WidgetBuilder>{
        '/Calc' : (Context) => Calc(),
        '/hasil' : (Context) => hasil(),
        '/hist' : (Context) => hist(),
        '/loc' : (Context) => Loc(),
      },
    );
  }
}

//di bawah ini adalah untuk halaman utama
class halamanU extends StatelessWidget{

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),

      body: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            children: <Widget>[
              Center(
                child: Text("Selamat Datang di Kalkulator Fungsi Rumus ABC"),
              ),
              //Di bawah ini adalah tombol masuk ke fungsi menghitung!
              TextButton(
                child: Text("Masuk ke kakulator"),
                style: TextButton.styleFrom(
                  foregroundColor: Colors.white, backgroundColor: Colors.blue,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/Calc');
                },
              ),
              //Di bawah ini adalah tombol ke history hitungan!
              TextButton(
                child: Text("Masuk ke History"),
                style: TextButton.styleFrom(
                  foregroundColor: Colors.white, backgroundColor: Colors.blue,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/hist');
                },
              ),
              //Di bawah ini adalah tombol cek lokasi!
              TextButton(
                child: Text("Cek Lokasi Anda!"),
                style: TextButton.styleFrom(
                  foregroundColor: Colors.white, backgroundColor: Colors.blue,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/loc');
                },
              ),
            ],
          )
      ),
    );
    //Scaffold berfungsi sebagai halaman dasar untuk menampung appBar dan body
  }
}

//halaman calculator
class Calc extends StatelessWidget{
  TextEditingController nilaiA = TextEditingController();
  TextEditingController nilaiB = TextEditingController();
  TextEditingController nilaiC = TextEditingController();
  static String n1 = "";
  static String n2 = "";


  //ini untuk halaman tombol pertama di home
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Kalkulator Rumus ABC"),
      ),

      body: Padding(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Center(
              child:Text("Masukkan nilai abc yang ada di fungsi"),
            ),
            Center(
              child:Text("Contoh: aX^2 + bX - c"),
            ),
            Padding(padding: EdgeInsets.all(15),
              child: TextField(
                controller: nilaiA,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Angka 1',
                  hintText: 'Masukkan Nilai a',
                ),
              ),),
            Padding(padding: EdgeInsets.all(15),
              child: TextField(
                controller: nilaiB,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Angka 2',
                  hintText: 'Masukkan Nilai b',
                ),
              ),),
            Padding(padding: EdgeInsets.all(15),
              child: TextField(
                controller: nilaiC,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Angka 3',
                  hintText: 'Masukkan Nilai c',
                ),
              ),),
            ElevatedButton(
              child: Text('Hitung'),
              onPressed: (){
                double a = double.parse(nilaiA.text);
                double b = double.parse(nilaiB.text);
                double c = double.parse(nilaiC.text);

                double x = sqrt(b*b-4*a*c);
                double y = 2*a;
                double nd1 = (-b+x)/y;
                double nd2 = (-b-x)/y;
                n1 = nd1.toString();
                n2 = nd2.toString();

                Navigator.pushNamed(context, '/hasil');
              },
            ),
          ],
        ),
      ),

    );
    //Scaffold berfungsi sebagai halaman dasar untuk menampung appBar dan body
  }
}

class hasil extends StatelessWidget {
  //dibawah ini untuk ambil nilai variabel di class Calc
  String n1 = Calc.n1;
  String n2 = Calc.n2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Kalkulator Rumus ABC"),
      ),

      body: Padding(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Center(
              child:Text("Nilai x1 = " + n1 + " dan nilai x2 = " + n2),
            ),
          ],
        ),
      ),

    );
    //Scaffold berfungsi sebagai halaman dasar untuk menampung appBar dan body
  }
}

//Halaman history
class hist extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("History Hitungan"),
      ),

      body: Padding(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Center(
              child:Text("Masih kosong."),
            ),
          ],
        ),
      ),

    );
    //Scaffold berfungsi sebagai halaman dasar untuk menampung appBar dan body
  }
}

class Loc extends StatelessWidget {
  void checkLocationPermission(BuildContext context) async {
    PermissionStatus status = await Permission.location.request();

    if (status.isGranted) {
      Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
      );

      if (position != null) {
        double latitude = position.latitude;
        double longitude = position.longitude;

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MapScreen(latitude: latitude, longitude: longitude),
          ),
        );
      }
    } else if (status.isDenied) {
      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text('Izin Ditolak'),
          content: Text('Izin lokasi ditolak. Aktifkan izin lokasi di pengaturan perangkat!'),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
    } else if (status.isPermanentlyDenied) {
      showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
          title: Text('Izin Ditolak Secara Permanen.'),
          content: Text('Izin lokasi ditolak secara permanen. Buka pengaturan perangkat untuk mengaktifkan izin lokasi!'),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("Lokasi Anda."),
      ),
      body: Padding(
        padding: EdgeInsets.all(15),
        child: Column(
          children: <Widget>[
            Center(
              child: Text("Ini Lokasi Anda Saat Ini!"),
            ),
            ElevatedButton(
              child: Text('Cek Lokasi'),
              onPressed: () {
                checkLocationPermission(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class MapScreen extends StatefulWidget {
  final double latitude;
  final double longitude;

  const MapScreen({Key? key, required this.latitude, required this.longitude}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  late GoogleMapController _controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Peta Lokasi'),
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(widget.latitude, widget.longitude),
          zoom: 17,
        ),
        onMapCreated: (controller) {
          setState(() {
            _controller = controller;
          });
        },
        markers: {
          Marker(
            markerId: MarkerId('currentLocation'),
            position: LatLng(widget.latitude, widget.longitude),
            infoWindow: InfoWindow(title: 'Lokasi Anda'),
          ),
        },
      ),
    );
  }
}

